module.exports = [
  {
    _id: 1,
    name: 'GG Guys',
    cpf: '117.716.120-61',
    birthdate: '1997-02-15',
  },
  {
    _id: 2,
    name: 'Name Teste',
    cpf: '949.653.340-00',
    birthdate: '1997-07-21',
  },
  {
    _id: 3,
    name: 'Araya 123',
    cpf: '020.993.980-03',
    birthdate: '1997-07-25',
  },
  {
    _id: 4,
    name: '123',
    cpf: '288.311.830-28',
    birthdate: '1997-02-24',
  },
  {
    _id: 5,
    name: 'Login BB teste dados',
    cpf: '968.753.540-71',
    birthdate: '1997-07-21',
  },
  {
    _id: 6,
    name: 'E-mail duplicado',
    cpf: '445.202.410-65',
    birthdate: '1997-07-21',
  }
]