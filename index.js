const express = require('express');
const cors = require('cors');
const app = express();
const PORT = 3000;
const FakeUsers = require('./users');
const JWT = require('jsonwebtoken');
const responseCreditSumulator = require('./response');
const responseCreditSumulator2 = require('./response2');


app.use(cors('*'));

function generateToken(data) {
    return JWT.sign({ ...data }, 'userInfoKey', { expiresIn: '1h' });
}

app.listen(PORT, (connection) => {
    console.log(`Connection on port: ${PORT}`);
})

app.get('/ui/authorize', (req, res) => {
    console.log("> call!");
    const { redirect_uri } = req.query;
    const redirect = `${redirect_uri}?code=123`;
    return res.redirect(redirect);
});

app.post('/oauth/token', (req, res) => {
    console.log('Auth token');
    return res.json({
        access_token: '123'
    });
});

app.post('/oauth/userinfo', (req, res) => {
    console.log('User info');
    return res.send(generateToken(FakeUsers[3]));
});

app.post('/simular-operacao-credito-linha-sugerida', (req, res) => {
    return res.json(responseCreditSumulator);
});

app.post('/consultar-condicoes-linhas-credito', (req, res) => {
    return res.json(responseCreditSumulator2);
});

